---
title:      "Plasma Mobile update: September 2020"
created_at: 2020-10-09 09:45:00 UTC
author:     Plasma Mobile team
layout:     post
---

The Plasma Mobile team is happy to present the Plasma Mobile updates from the month of September. This month's update includes various improvements and bugfixes in file dialogs, the virtual keyboard, lockscreen, various applications, and updates from KDE's annual conference, Akademy.

## File Dialogs

Jonah Brüchert has implemented support for forwarding file dialog requests to the xdg-desktop-portal in plasma-integration.
plasma-integration is a Plasma Qt plugin that provides all kinds of system dialogs, like file dialogs.
Sandboxed apps can't use plasma-integration, so xdg-desktop-portal-kde was born to provide system dialogs for them. It is not a Qt plugin, but a system service that apps can call if they need such a dialog.
With the latest patch, plasma-integration can just call xdg-desktop-portal, so dialogs only have to be implemented there once.
In short, all apps are now using the xdg-desktop-portal, no matter whether they are sandboxed or not.

{% section {"app_name": "Virtual Keyboard", "app_img": "/img/maliit-keyboard-picture.jpg", "app_img_shadow": "true", "app_img_alt": "Plasma Mobile Virtual Keyboard, maliit"} %}

We switched the Virtual Keyboard in Plasma Mobile to [maliit2](https://maliit.github.io/news/2017/05/24/maliit-keyboard/). Several changes were made across various repositories for this change:

- Aleix Pol integrated the input-method-unstable-v1 protocol in [kwayland-server](https://invent.kde.org/plasma/kwayland-server/-/merge_requests/31) and [kwin](https://invent.kde.org/plasma/kwin/-/merge_requests/106).
- Aleix Pol also switched Plasma Mobile to use maliit instead of the QtVirtualKeyboard in [plasma-phone-components](https://invent.kde.org/plasma/plasma-phone-components/-/merge_requests/82)
- Bhushan Shah created a [patch](https://github.com/maliit/keyboard/pull/6) that fixed an issue in the maliit-keyboard to make it use the correct wayland shell-integration plugin.
- Marius Gripsgard from UBports fixed the maliit-keyboard for usage [under wayland](https://github.com/maliit/keyboard/pull/9) .
- [Marco Martin created a patch](https://github.com/maliit/keyboard/pull/14) that resizes the virtual keyboard if the screen resolution changes.
- Marius Gripsgard from UBports updated the input-method area even when the keyboard is hidden, which provides a [smoother animation](https://github.com/maliit/framework/pull/44).
- [Bhushan Shah created a patch](https://invent.kde.org/plasma/kwin/-/merge_requests/212) that sends surrounding text and content type hints to the input method.
- [Bhushan Shah created a patch](https://invent.kde.org/plasma/kwin/-/merge_requests/233) that closes the keyboard when the focused text input goes away. 
- Bhushan Shah added text-input-v3 support in [kwayland-server](https://invent.kde.org/plasma/kwayland-server/-/merge_requests/90) and [kwin](https://invent.kde.org/plasma/kwin/-/merge_requests/293), enabling virtualkeyboard for GTK3 applications.

There's several minor issues with the new virtual keyboard, but we plan to solve them in the upcoming days.

{% endsection %}

## Lockscreen

Devin Lin has rewritten the lockscreen and merged the changes.

![New lockscrenn](/img/new-lockscreen.gif){:class="img-fluid"}

The main time and date text has been shortened and the sizing has been changed to take up less space on the screen.

A new tablet layout has been added so that everything scales nicely on different display sizes.

A new keypad has also been added with blurs behind it. This enhances its looks and it also shows a consistent sizing on different display sizes.

The indicator panel has been added to the top of the lockscreen.

Very basic notifications were also added to the lockscreen. We will need more discussion to determine permissions for actions on the notifications.

## KClock

KClock has been split into a daemon and client app to reduce the memory usage of always having the full qml runtime in the background. The timer page has been rewritten to use fluid animations throughout. Issues with timers causing crashes have also been fixed.

{% section {"app_name": "KWeather", "app_img": "/img/kweather-animated-background.png", "app_img_shadow": "false", "app_img_alt": "KWeather, weather application"} %}

The KWeather development team is working on bringing the app up to the fanciness of its counterparts on Android/iOS. Current work is being done on making an animated background for different weathers, with the option of using the current flat style as well. Weather alerts are also planned. This shouldn't affect those of you who use the stable release, but the Plasma Mobile neon image is updated on a daily basis, so the plasmoid and other things may break.

{% endsection %}

{% section {"app_name": "Arkade", "app_img": "/img/akarde_samegame.png", "app_img_shadow": "true", "app_img_alt": "Arkade, collection of games written in QtQuick"} %}

Arkade is a collection of games written in QtQuick. The goal of Arkade is to provide some basic functionalities like high scores and archievements. This is still in quite early stages of development, but here are some screenshots from the progress so far.

{% endsection %}

<figure class="text-center">
  <img src="/img/arkade_connect.png" class="shadow blog-post-image-small" alt="" class="img-fluid" style="width: 700px" />
  <figcaption class="mt-2">Connect Four Game</figcaption>
</figure>

<figure class="text-center">
  <img src="/img/arkade_blocks.png" class="shadow blog-post-image-small" alt="" class="img-fluid" style="max-width: 700px" />
  <figcaption class="mt-2">Tetris like game</figcaption>
</figure>


## Conference and Events

As announced in our last blog post, Bhushan Shah gave a talk about ["Plasma on Mobile devices"](https://linuxplumbersconf.org/event/7/contributions/835/) at the [Linux Plumbers Conference](https://www.linuxplumbersconf.org/). You can find the [slides](https://linuxplumbersconf.org/event/7/contributions/835/attachments/692/1278/plamo-lpc-2020-applications-ecosystem-mc.pdf) and the [recording of his talk online](https://youtu.be/4RZhTRZYb8I?t=6214).

At the beginning of September, [Akademy](https://akademy.kde.org), KDE's annual community, was held online. There were some talks about applications built with Kirigami and also about developing applications with Kirigami:

* Camilo Higuita's ["Presentation: Maui Project Update"](https://conf.kde.org/en/akademy2020/public/events/238) provided information about the current state and the future of the Maui project.

* leinir and andrewshoben described in their talk [how they built a tail remote app using Kirigami](https://conf.kde.org/en/akademy2020/public/events/230) ([slides](https://conf.kde.org/system/event_attachments/attachments/000/000/139/original/kde-wags-your-tail.pdf))

* Luis Falcon presented a talk in which he explained [how Kirigami helped him build MyGNUHealth](https://conf.kde.org/en/akademy2020/public/events/255), the mobile application for the GNU Health system.

* Dimitris Kardarakos explained [how to create a convergent application with Kirigami](https://conf.kde.org/en/akademy2020/public/events/254) using the calendar application Calindori as an example.

* Patrick Pereira [demonstrated two tools that can help with rapid prototyping of QML](https://conf.kde.org/en/akademy2020/public/events/218).

On Wednesday, the current state and the future developments of Plasma Mobile were discussed at the Plasma Mobile BoF.

On Friday, [Bhushan Shah was awarded one of the Akademy Awards](https://community.kde.org/Akademy/Awards#2020) for his work on creating an entire new platform for applications, Plasma Mobile.

## Want to help?

This post shows what a difference new contributors can make. Do you want to be part of our team as well? We prepared [a few ideas for tasks new contributors](https://invent.kde.org/dashboard/issues/?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Mobile&label_name[]=Junior%20Job) can work on. Most coding tasks require some QML knowledge. [KDAB's](https://www.kdab.com/) [QML introduction video series](https://youtu.be/JxyTkXLbcV4) is a great resource for that!

You can join us on various communication channels using the links at [https://www.plasma-mobile.org/join/](https://www.plasma-mobile.org/join/).
