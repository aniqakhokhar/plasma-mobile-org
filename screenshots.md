---
title: Screenshots
permalink: /screenshots/
layout: page
screenshots:
  - name: Plasma
    url: /img/screenshots/shell.png
  - name: Angelfish
    url: /img/screenshots/angelfish.png
  - name: Kaidan
    url: /img/screenshots/kaidan_1.png
  - name: Pure Maps
    url: /img/screenshots/puremaps.png
  - name: Okular
    url: /img/screenshots/okular.png
  - name: Koko
    url: /img/screenshots/koko.png
  - name: Index
    url: /img/screenshots/index.png
  - name: Calindori
    url: /img/screenshots/calindori.png
  - name: Discover
    url: /img/screenshots/discover.png
  - name: Angelfish
    url: /img/screenshots/angelfish_2.png
  - name: Kalgebra
    url: /img/screenshots/kalgebra.png
  - name: Camera
    url: /img/screenshots/camera.png
  - name: Kirigami Gallery
    url: /img/screenshots/kirigami_gallery.png
  - name: Angelfish
    url: /img/screenshots/angelfish_3.png
  - name: Kaidan
    url: /img/screenshots/kaidan_2.png
  - name: Calindori
    url: /img/screenshots/calindori_2.png

---

# Screenshots

The following screenshots were taken from a Nexus 5X mobile device running Plasma Mobile.

<style>.swiper-container, .swiper-slide img, .screenshot img { height: 50vw; min-height: 300px; max-height: 500px; } .swiper-pagination-bullet { width: 12px; height: 12px; } .swiper-slide { flex-shrink: initial; } main {overflow-y: hidden }</style>

<section class="swiper-container d-flex swiper-container-initialized swiper-container-horizontal" aria-label="Screenshots role=" list="">
  {% for screenshot in page.screenshots %}
    <div class="swiper-wrapper d-flex my-3" role="listitem">
      <div class="swiper-slide {% if forloop.first %}swiper-slide-active{% endif %}" style="margin-right: 30px;">
        <img src="{{ screenshot.url }}" alt="{{ screenshot.name }}">
      </div>
    </div>
  {% endfor %}
  <div class="swiper-pagination swiper-pagination-bullets"><span class="swiper-pagination-bullet swiper-pagination-bullet-active"></span><span class="swiper-pagination-bullet"></span><span class="swiper-pagination-bullet"></span><span class="swiper-pagination-bullet"></span><span class="swiper-pagination-bullet"></span><span class="swiper-pagination-bullet"></span></div>
  <div class="swiper-button-prev swiper-button-disabled" tabindex="0" role="button" aria-label="Previous slide" aria-disabled="true"></div>
  <div class="swiper-button-next" tabindex="0" role="button" aria-label="Next slide" aria-disabled="false"></div>
  <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
</section>

<script src="https://cdn.kde.org/aether-devel/version/kde-org/applications.3e16ae06.js"></script>
